# Live Coder

Live Coder shows how your code runs as you type.

![Demo GIF](https://gitlab.com/Fraser-Greenlee/live-coder-vscode-extension/raw/master/gifs/basic%20live%20coder.gif)

## Features

See the execution of all functions called for given test.

![GIF shows multiple function's execution syncing scrolling and changing files.](https://gitlab.com/Fraser-Greenlee/live-coder-vscode-extension/raw/master/gifs/traverse%20function%20calls.gif)

Pick between function calls and tests.

![GIF shows picking between fucntion calls and tests.](https://gitlab.com/Fraser-Greenlee/live-coder-vscode-extension/raw/master/gifs/traverse%20tests.gif)

## Usage

1. [Setup your python interpreter](###setup-your-python-interpreter)
2. [Configure your tests](###configure-your-tests)
3. Press <kbd>⇧</kbd> + <kbd>⌘</kbd> + <kbd>p</kbd> then select `Live Coder: Open`

### Setup your Python Interpreter

1. Ensure you have installed the Python extension
2. Press <kbd>⇧</kbd> + <kbd>⌘</kbd> + <kbd>p</kbd> then select "Python Select Interpreter"
3. Select a Python3 interpreter.

### Configure your tests

1. Press <kbd>⇧</kbd> + <kbd>⌘</kbd> + <kbd>p</kbd> then select "configure tests"
2. Select `unittests`
3. Enter your test folder & pattern.
4. Ensure you can run the test in VSCode.

### Requirements

Only runs Python3 unittests.

### Setup

1. Install the Extension.
2. Watch the [intro video](https://www.youtube.com/watch?v=LW_fgRFmEGI).
3. Try the [demo project](https://gitlab.com/Fraser-Greenlee/live-coder-demo-project).

## Having Issues?

Please [add an issue](https://gitlab.com/Fraser-Greenlee/live-coding).

## Thanks

Thanks to the [Pioneer](https://pioneer.app) community for the encouragement, it's worth checking out if you love making things!
If your interested in other new coding tools you should check out the [Future of Coding community](https://futureofcoding.org)!
