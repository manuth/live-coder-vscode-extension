import { glob } from "glob";
import * as Mocha from "mocha";
import * as path from "path";

export function run() {
    const testRoot = path.resolve(__dirname);

    const mocha = new Mocha(
        {
            ui: "tdd"
        });

    return new Promise<void>(
        (resolve, reject) => {
            glob(
                "**/*.test.js",
                {
                    cwd: testRoot
                },
                (error, matches) => {
                    if (error) {
                        reject(error);
                    }

                    matches.forEach((file) => mocha.addFile(path.resolve(testRoot, file)));

                    mocha.run(
                        (failures) => {
                            if (failures > 0) {
                                reject(new Error(`${failures} test${failures > 1 ? "s" : ""} failed!`));
                            } else {
                                resolve();
                            }
                        });
                });
        });
}
